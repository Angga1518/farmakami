from django import forms
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def produkDibeliChoice():
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_apotek from apotek order by id_apotek::int")
    apotek = namedtuplefetchall(cursor)
    cursor.execute("select id_produk from produk order by id_produk::int")
    produk = namedtuplefetchall(cursor)
    cursor.execute("select id_transaksi_pembelian from transaksi_pembelian order by id_transaksi_pembelian::int")
    transaksi_pembelian = namedtuplefetchall(cursor)
    apotek_choice = []
    produk_choice = []
    transaksi_pembelian_choice = []

    idx_string = ""

    for a in apotek:
        idx_string = str(a.id_apotek)
        apotek_choice.append((idx_string, idx_string))
        
    for a in produk:
        idx_string = str(a.id_produk)
        produk_choice.append((idx_string, idx_string))

    for a in transaksi_pembelian:
        idx_string = str(a.id_transaksi_pembelian)
        transaksi_pembelian_choice.append((idx_string, idx_string))

    argument = [
        apotek_choice,
        produk_choice,
        transaksi_pembelian_choice
    ]
    return argument

def pengantaranFarmasiChoice():
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_transaksi_pembelian from transaksi_pembelian order by id_transaksi_pembelian::int")
    transaksi_pembelian = namedtuplefetchall(cursor)
    cursor.execute("select id_kurir from kurir order by id_kurir::int")
    kurir = namedtuplefetchall(cursor)
    transaksi_pembelian_choice = []
    kurir_choice = []

    idx_string = ""

    for a in kurir:
        idx_string = str(a.id_kurir)
        kurir_choice.append((idx_string, idx_string))
        
    for a in transaksi_pembelian:
        idx_string = str(a.id_transaksi_pembelian)
        transaksi_pembelian_choice.append((idx_string, idx_string))

    argument = [
        transaksi_pembelian_choice,
        kurir_choice
    ]

    return argument

choicesProdukDibeli = produkDibeliChoice()
choicesPengantaranFarmasi = pengantaranFarmasiChoice()

class CreateProdukDibeli(forms.Form):
    id_apotek = forms.CharField(label = 'ID Apotek', required = True, widget=forms.Select(choices=choicesProdukDibeli[0]))
    id_produk = forms.CharField(label = 'ID Produk', required = True, widget=forms.Select(choices=choicesProdukDibeli[1]))
    id_transaksi_pembelian = forms.CharField(label = 'ID Transaksi Pembelian', required = True, widget=forms.Select(choices=choicesProdukDibeli[2]))
    jumlah = forms.IntegerField(label = 'Jumlah yang Dibeli')

class UpdateProdukDibeli(forms.Form):
    id_apotek = forms.CharField(label = 'ID Apotek', widget = forms.Select(attrs={'readonly':'readonly'}, choices=choicesProdukDibeli[0]), required = True)
    id_produk = forms.CharField(label = 'ID Produk', widget = forms.Select(attrs={'readonly':'readonly'}, choices=choicesProdukDibeli[1]), required = True)
    id_transaksi_pembelian = forms.CharField(label = 'ID Transaksi Pembelian', required = True, widget=forms.Select(choices=choicesProdukDibeli[2]))
    jumlah = forms.IntegerField(label = 'Jumlah yang Dibeli', widget = forms.NumberInput(), initial = 10)

class CreatePengantaranFarmasi(forms.Form):
    id_kurir = forms.CharField(label = 'ID Kurir', widget = forms.Select(choices=choicesPengantaranFarmasi[1]), required = True)
    id_transaksi_pembelian = forms.CharField(label = 'ID Transaksi Pembelian', widget=forms.Select(choices=choicesPengantaranFarmasi[0]), required = True)
    waktu = forms.DateTimeField(label = 'Tanggal dan Waktu (Format yyyy-mm-dd hh:mm:ss)', required = True, initial="Now")
    biaya_kirim = forms.IntegerField(label = 'Biaya Kirim', required = True, initial = 0)

class UpdatePengantaranFarmasi(forms.Form):
    id_pengantaran = forms.CharField(label = 'ID Pengantaran', widget = forms.Select(choices=choicesPengantaranFarmasi[0]), required = True)
    id_kurir = forms.CharField(label = 'ID Kurir', widget = forms.Select(choices=choicesPengantaranFarmasi[1]), required = True)
    id_transaksi_pembelian = forms.CharField(label = 'ID Transaksi Pembelian', widget = forms.Select(choices=choicesPengantaranFarmasi[0]), required = True)
    waktu = forms.DateTimeField(label = 'Tanggal dan Waktu', required = True)
    status_pengantaran = forms.CharField(label = 'Status Pengantaran', required = True)
    biaya_kirim = forms.IntegerField(label = 'Biaya Kirim', required = True)