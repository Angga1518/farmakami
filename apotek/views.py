from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError

#GENERAL FUNCTION
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

#GET VALUE FUNCTION
def get_input_apotek(request):
    input = request.POST
    nama_apotek = input['namaApotek']
    alamat_apotek = input['alamatApotek']
    telepon_apotek = input['telpApotek']
    nama_penyelenggara = input['namaPenyelenggara']
    no_sia = input['siaPenyelenggara']
    email = input['emailPenyelenggara']
    return [nama_apotek, alamat_apotek, telepon_apotek, nama_penyelenggara, no_sia, email]

#CREATE APOTEK FUNCTION
def createApotek(request):
    message =""
    if request.method =='POST':
        input = get_input_apotek(request)
        print(input[0])
        cursor = connection.cursor()
        cursor.execute("set search_path to farmakami")
        cursor.execute("select id_apotek as c from apotek order by id_apotek::int asc")
        id_apotek = namedtuplefetchall(cursor)
        print(id_apotek)
        id_apotek = id_apotek[-1].c
        id_apotek = int(id_apotek) + 1
        idString = str(id_apotek)
        try :
            cursor.execute("insert into apotek values ('"+idString+"','"+input[5]+"','"+input[4]+"','"+input[3]+"','"+input[0]+"','"+input[1]+"','"+input[2]+"')")
            cursor.close()
            return redirect('/apotek/tableApotek')
        except Exception as e:
            message = str(e)[79:]
            if "alamat" in message :
                message = message[7:]

    form = apotekForms()
    args = {
        'form': form,
        'message' : message
        }
    return render(request, 'createApotek.html', args)

#READ APOTEK FUNCTION
def tableApotek(request):
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from apotek order by id_apotek::int")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'roleAdmin' : roleAdmin,
    }
    return render(request, "tableApotek.html", argument)

#UPDATE APOTEK FUNCTION
def updateApotek(request, id_apotek, success=True):
    message =""
    id_apotek = str(id_apotek)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from apotek where id_apotek = '"+id_apotek+"'")
    hasil = namedtuplefetchall(cursor)
    id = hasil[0].id_apotek
    namaApotek = hasil[0].nama_apotek
    alamatApotek = hasil[0].alamat_apotek
    telpApotek = hasil[0].telepon_apotek
    namaPenyelenggara = hasil[0].nama_penyelenggara
    siaPenyelenggara = hasil[0].no_sia
    emailPenyelenggara = hasil[0].email
    form = updateApotekForms(initial={
        'idApotek': id_apotek, 'namaApotek': namaApotek, 'alamatApotek': alamatApotek, 'telpApotek': telpApotek, 'namaPenyelenggara': namaPenyelenggara, 'siaPenyelenggara': siaPenyelenggara, 'emailPenyelenggara': emailPenyelenggara })
    if request.method == 'POST':
        input = get_input_apotek(request)
        print(input)
        try :
            cursor.execute("update apotek set email ='"+input[5]+"', no_sia ='"+input[4]+"', nama_penyelenggara ='"+input[3]+"', nama_apotek ='"+input[0]+"', alamat_apotek ='"+input[1]+"', telepon_apotek ='"+input[2]+"' where id_apotek ='"+id_apotek+"'")
            cursor.close()
            return redirect('/apotek/tableApotek')
        except Exception as e:
            message = str(e)[79:]
            if "alamat" in message :
                message = message[7:]
    argument = {
        'form' : form,
        'message' : message,
        'id' : id,
    }
    return render(request, "updateApotek.html", argument)

#DELETE APOTEK FUNCTION
def deleteApotek(request, id_apotek):
    id_apotek = str(id_apotek)
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from apotek where id_apotek ='"+id_apotek+"'")
    cursor.execute("select * from apotek order by id_apotek::int")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil terhapus"
    argument = {
        'table' : hasil,
        'roleAdmin' : roleAdmin,
        'message' : message
    }
    return render(request, "tableApotek.html", argument)
