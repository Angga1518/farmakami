from django import forms

class apotekForms(forms.Form):
    namaApotek = forms.CharField(label='Nama Apotek', max_length=100, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    alamatApotek = forms.CharField(label='Alamat Apotek', widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    telpApotek = forms.CharField(label='Telepon Apotek', max_length=20, required=False)
    namaPenyelenggara = forms.CharField(label='Nama Penyelenggara', max_length=50, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    siaPenyelenggara = forms.CharField(label='No. SIA penyelenggara', max_length=20, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    emailPenyelenggara = forms.CharField(label='Email Penyelenggara', max_length=50, widget=forms.TextInput(attrs={'placeholder':'*Required'}))

class updateApotekForms(forms.Form):
    idApotek = forms.CharField(label='ID Apotek', max_length=10,disabled = True, widget = forms.TextInput(attrs={'readonly':'readonly'}))
    namaApotek = forms.CharField(label='Nama Apotek', max_length=100)
    alamatApotek = forms.CharField(label='Alamat Apotek')
    telpApotek = forms.CharField(label='Telepon Apotek', max_length=20, required=False)
    namaPenyelenggara = forms.CharField(label='Nama Penyelenggara', max_length=50)
    siaPenyelenggara = forms.CharField(label='No. SIA penyelenggara', max_length=20)
    emailPenyelenggara = forms.CharField(label='Email Penyelenggara', max_length=50)
