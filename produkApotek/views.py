from django.shortcuts import render, reverse, redirect
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError

#GENERAL FUNCTION
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

#GET VALUE FUNCTION
def get_input_produk_apotek(request):
    input = request.POST
    harga_jual = input['hargaJual']
    satuan_penjualan = input['satuanPenjualan']
    stok = input['stok']
    return [harga_jual, satuan_penjualan, stok]

#CREATE PRODUK APOTEK FUNCTION
def createProdukApotek(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_apotek from apotek order by id_apotek asc")
    id_apotek_view = namedtuplefetchall(cursor)
    cursor.execute("select id_produk from produk order by cast(id_produk as int)")
    id_produk_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        input = get_input_produk_apotek(request)
        id_produk = request.POST['id_produk']
        id_apotek = request.POST['id_apotek']
        try :
            cursor.execute("insert into produk_apotek values ('"+input[0]+"','"+input[2]+"','"+input[1]+"','"+id_produk+"','"+id_apotek+"')")
            cursor.close()
            return redirect('/produkApotek/tableProdukApotek')
        except Exception as e:
            message = str(e)[80:]

    form = produkApotekForms()
    args = {
        'id_apotek': id_apotek_view,
        'id_produk': id_produk_view,
        'form': form,
        'message' : message
        }
    return render(request, "createProdukApotek.html", args)

#READ PRODUK APOTEK FUNCTION
def tableProdukApotek(request):
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from produk_apotek order by cast(id_produk as int), id_apotek asc")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'roleAdmin' : roleAdmin,
    }
    return render(request, "tableProdukApotek.html", argument)

#UPDATE PRODUK APOTEK FUNCTION
def updateProdukApotek(request, id_apotek, id_produk, success=True):
    message =""
    id_apotek = str(id_apotek)
    id_produk = str(id_produk)
    id_produk_new = str(id_apotek)
    id_apotek_new = str(id_produk)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_apotek from apotek order by id_apotek asc")
    id_apotek_view = namedtuplefetchall(cursor)
    cursor.execute("select id_produk from produk order by cast(id_produk as int)")
    id_produk_view = namedtuplefetchall(cursor)
    cursor.execute("select * from produk_apotek where id_apotek = '"+id_apotek+"' and id_produk = '"+id_produk+"'")
    hasil = namedtuplefetchall(cursor)
    hargaJual = hasil[0].harga_jual
    stok = hasil[0].stok
    satuanPenjualan = hasil[0].satuan_penjualan
    form = updateProdukApotekForms(initial={
        'hargaJual': hargaJual, 'satuanPenjualan': satuanPenjualan, 'stok': stok })
    
    if request.method == 'POST':
        input = get_input_produk_apotek(request)
        id_produk_new = request.POST['id_produk']
        id_apotek_new = request.POST['id_apotek']
        print(id_apotek_new)
        try :
            cursor.execute("update produk_apotek set harga_jual ='"+input[0]+"', stok ='"+input[2]+"', satuan_penjualan ='"+input[1]+"', id_produk='"+id_produk_new+"', id_apotek='"+id_apotek_new+"' where id_produk ='"+id_produk+"' and id_apotek ='"+id_apotek+"'")
            cursor.close()
            return redirect('/produkApotek/tableProdukApotek')
        except Exception as e:
            message = str(e)[80:]
            
    argument = {
        'form' : form,
        'message' : message,
        'idApotekForm' : id_apotek,
        'idProdukForm' : id_produk,
        'id_apotek': id_apotek_view,
        'id_produk': id_produk_view,
    }
    return render(request, "updateProdukApotek.html", argument)

#DELETE PRODUK APOTEK FUNCTION
def deleteProdukApotek(request, id_apotek, id_produk):
    id_apotek = str(id_apotek)
    id_produk = str(id_produk)
    cursor = connection.cursor()
    roleAdmin = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from produk_apotek where id_apotek = '"+id_apotek+"' and id_produk = '"+id_produk+"'")
    cursor.execute("select * from produk_apotek order by cast(id_produk as int), id_apotek asc")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil terhapus"
    argument = {
        'table' : hasil,
        'roleAdmin' : roleAdmin,
        'message' : message
    }
    return render(request, "tableProdukApotek.html", argument)