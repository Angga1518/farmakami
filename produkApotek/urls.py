from django.urls import path, include
from django.contrib import admin
from .views import createProdukApotek, tableProdukApotek, updateProdukApotek, deleteProdukApotek

app_name = "produkApotek"
urlpatterns = [
    path('createProdukApotek', createProdukApotek, name="createProdukApotek"),
    path('tableProdukApotek', tableProdukApotek, name="tableProdukApotek"),
    path('updateProdukApotek/<str:id_apotek>/<str:id_produk>', updateProdukApotek, name="updateProdukApotek"),
    path('deleteProdukApotek/<str:id_apotek>/<str:id_produk>', deleteProdukApotek, name="deleteProdukApotek")
]