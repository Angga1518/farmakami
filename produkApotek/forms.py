from django import forms

class produkApotekForms(forms.Form):
    hargaJual = forms.DecimalField(label='Harga Jual', min_value=0, initial=1000, widget=forms.NumberInput(attrs={'placeholder':'*Required'}))
    satuanPenjualan = forms.CharField(label='Satuan Penjualan', max_length=5, widget=forms.TextInput(attrs={'placeholder':'*Required'}))
    stok = forms.IntegerField(label='Stok', min_value=0, widget=forms.NumberInput(attrs={'placeholder':'*Required'}))


class updateProdukApotekForms(forms.Form):
    hargaJual = forms.DecimalField(label='Harga Jual', min_value=0, initial=1000)
    satuanPenjualan = forms.CharField(label='Satuan Penjualan', max_length=5)
    stok = forms.IntegerField(label='Stok', min_value=0)
