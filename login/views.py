from django.shortcuts import render, redirect
from .forms import LoginForm, ChoiceRoleForm, AdminRoleForm, KonsumenRoleForm, KurirRoleForm, CSRoleForm
from django.db import connection
from collections import namedtuple
from random import randrange

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request, validasi = None):
    try:
        email = request.session['email']
        return login(request)
    except KeyError:
        formulir = LoginForm()
        message = ""
        if(validasi==False):
            message = "Invalid email or password"
        argument = { 
            'formlogin' : formulir,
            'message' : message
        }
        return render(request, 'login.html', argument)
    
def login(request):
    try:
        email = request.session['email']
        password = request.session['password']
    except:
        email = request.POST['email']
        password = request.POST['password']

    email = str(email)
    password = str(password)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from pengguna where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)
    role = cekRole(email)
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    if (role == 'Admin'):
        roleAdmin = True
    if (role == 'Konsumen'):
        roleKonsumen = True
    if (role == 'Kurir'):
        roleKurir = True
    if (role == 'CS'):
        roleCS = True

    cursor.execute("set search_path to public")
    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        request.session['email'] = hasil[0].email
        request.session['telepon'] = hasil[0].telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = role
        argument = {
            'hasil' : hasil,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }
        cursor.close()
        return render(request, "home.html", argument)

def logout(request):
    request.session.flush()
    request.session.clear_expired()
    return index(request)

def profil(request):
    try:
        email = request.session['email']
    except Exception as e:
        return redirect('/')
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from pengguna where email='"+email+"'")
    pengguna = namedtuplefetchall(cursor)

    role = cekRole(email)
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    if (role == 'Admin'):
        roleAdmin = True
    if (role == 'Konsumen'):
        roleKonsumen = True
    if (role == 'Kurir'):
        roleKurir = True
    if (role == 'CS'):
        roleCS = True
        
    if (roleAdmin):
        cursor.execute("select id_apotek from admin_apotek where email='"+email+"'")
        id_apotek = namedtuplefetchall(cursor)
        id_apotek = id_apotek[0].id_apotek

        argument = {
            'hasil' : pengguna,
            'role' : role,
            'id_apotek' : id_apotek,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }
    elif (roleKurir):
        cursor.execute("select * from kurir where email='"+email+"'")
        kurir = namedtuplefetchall(cursor)
        id_kurir = kurir[0].id_kurir
        nama_perusahaan = kurir[0].nama_perusahaan
        argument = {
            'hasil' : pengguna,
            'role' : role,
            'id_kurir' : id_kurir,
            'nama_perusahaan' : nama_perusahaan,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }
    elif (roleKonsumen):
        cursor.execute("select * from konsumen where email='"+email+"'")
        konsumen = namedtuplefetchall(cursor)
        id_konsumen = str(konsumen[0].id_konsumen)
        jenis_kelamin = konsumen[0].jenis_kelamin
        tanggal_lahir = konsumen[0].tanggal_lahir
        cursor.execute("select * from alamat_konsumen where id_konsumen='"+id_konsumen+"'")
        data_alamat = namedtuplefetchall(cursor)
        argument = {
            'hasil' : pengguna,
            'role' : role,
            'id_konsumen' : id_konsumen,
            'jenis_kelamin' : jenis_kelamin,
            'tanggal_lahir' : tanggal_lahir,
            'data_alamat' : data_alamat,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }
    elif (roleCS):
        cursor.execute("select * from cs where email='"+email+"'")
        cs = namedtuplefetchall(cursor)
        no_ktp = cs[0].no_ktp
        no_sia = cs[0].no_sia
        argument = {
            'hasil' : pengguna,
            'role' : role,
            'no_ktp' : no_ktp,
            'no_sia' : no_sia,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }
    else:
        argument = {
            'hasil' : pengguna,
            'role' : role,
            'roleAdmin' : roleAdmin,
            'roleKonsumen' : roleKonsumen,
            'roleKurir' : roleKurir,
            'roleCS' : roleCS
        }

    cursor.close()
    return render(request, "profil.html", argument)

    
def cekRole(email):
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from pengguna where email='"+email+"'")
    pengguna = namedtuplefetchall(cursor)
    cursor.execute("select * from admin_apotek where email='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        cursor.close()
        return "Admin"
    else:
        cursor.execute("select * from kurir where email='"+email+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil != []):
            cursor.close()
            return "Kurir"
        else:
            cursor.execute("select * from cs where email='"+email+"'")
            hasil = namedtuplefetchall(cursor)
            if (hasil != []):
                cursor.close()
                return "CS"
            else:
                cursor.execute("select * from konsumen where email='"+email+"'")
                hasil = namedtuplefetchall(cursor)
                if (hasil != []):
                    cursor.close()
                    return "Konsumen"

def registerPengguna(request):
    formulir = ChoiceRoleForm()
    argument = { 
        'form' : formulir,
        'formrole' : True,
        'roleAdmin' : False,
        'roleKonsumen' : False,
        'roleKurir' : False,
        'roleCS' : False
    }
    return render(request, 'registerPengguna.html', argument)

def registerPenggunaRole(request, message="", role=None):
    try:
        Role = str(request.POST['Role'])
    except:
        Role = role
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    if(Role == "Admin"):
        formulir = AdminRoleForm()
        roleAdmin = True
    elif(Role == "Konsumen"):
        formulir = KonsumenRoleForm()
        roleKonsumen = True
    elif(Role == "Kurir"):
        formulir = KurirRoleForm()
        roleKurir = True
    else :
        formulir = CSRoleForm()
        roleCS = True

    argument = { 
        'form' : formulir,
        'role' : Role,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS,
        'message': message
    }
    return render(request, 'registerPengguna.html', argument)

def insertAdmin(request):
    email = str(request.POST['email'])
    password = str(request.POST['password'])
    nama_lengkap = str(request.POST['nama_lengkap'])
    no_telp = str(request.POST['no_telp'])

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select count(*) as c from apotek")
    hasil = namedtuplefetchall(cursor)
    hasil2 = int(hasil[0].c)
    cursor.execute("select * from pengguna where email='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil == []):
        cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"')")
        cursor.execute("insert into apoteker values ('"+email+"')")
        while True:
            try:
                id_apotek = str(randrange(hasil2)+1)
                cursor.execute("insert into admin_apotek values ('"+email+"','"+id_apotek+"')")
                break
            except Exception as e:
                continue
    else:
        cursor.close()
        return registerPenggunaRole(request, "Email sudah digunakan", 'Admin')
                
    cursor.execute("select * from pengguna where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['email'] = hasil[0].email
        request.session['telepon'] = hasil[0].telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = 'Admin'
        argument = {
            'hasil' : hasil,
            'roleAdmin' : True,
            'roleKonsumen' : False,
            'roleKurir' : False,
            'roleCS' : False
        }
        cursor.close()
        return render(request, "home.html", argument)

def insertKurir(request):
    email = str(request.POST['email'])
    password = str(request.POST['password'])
    nama_lengkap = str(request.POST['nama_lengkap'])
    no_telp = str(request.POST['no_telp'])
    nama_perusahaan = str(request.POST['nama_perusahaan'])

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")

    cursor.execute("select id_kurir as c from kurir order by id_kurir::int asc")
    id_kurir = namedtuplefetchall(cursor)
    id_kurir = int(id_kurir[-1].c)
    id_kurir = id_kurir + 1
    idk = str(id_kurir)

    cursor.execute("select * from pengguna where email='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        cursor.close()
        return registerPenggunaRole(request, "Email sudah digunakan", 'Kurir')

                
    cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"')")
    cursor.execute("insert into kurir values ('"+idk+"','"+email+"','"+nama_perusahaan+"')")
    
    
    cursor.execute("select * from pengguna where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['email'] = hasil[0].email
        request.session['telepon'] = hasil[0].telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = 'Kurir'
        argument = {
            'hasil' : hasil,
            'roleAdmin' : False,
            'roleKonsumen' : False,
            'roleKurir' : True,
            'roleCS' : False
        }
        cursor.close()
        return render(request, "home.html", argument)

def insertCS(request):
    email = str(request.POST['email'])
    password = str(request.POST['password'])
    nama_lengkap = str(request.POST['nama_lengkap'])
    no_telp = str(request.POST['no_telp'])
    no_ktp = str(request.POST['no_ktp'])
    no_sia = str(request.POST['no_sia'])
    
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")

    cursor.execute("select * from pengguna where email='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil == []):
        cursor.execute("select no_ktp from cs where no_ktp='"+no_ktp+"'")
        hasil = namedtuplefetchall(cursor)
        if (hasil == []):
            cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"')")
            cursor.execute("insert into apoteker values ('"+email+"')")
            cursor.execute("insert into cs values ('"+no_ktp+"','"+email+"','"+no_sia+"')")
        else:
            cursor.close()
            return registerPenggunaRole(request, "No KTP CS sudah terdaftar", "CS")
    else:
        cursor.close()
        return registerPenggunaRole(request, "Email sudah digunakan", "CS")
                
    cursor.execute("select * from pengguna where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['email'] = hasil[0].email
        request.session['telepon'] = hasil[0].telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = 'CS'
        argument = {
            'hasil' : hasil,
            'roleAdmin' : False,
            'roleKonsumen' : False,
            'roleKurir' : False,
            'roleCS' : True
        }
        cursor.close()
        return render(request, "home.html", argument)
    
def insertKonsumen(request):
    email = str(request.POST['email'])
    password = str(request.POST['password'])
    nama_lengkap = str(request.POST['nama_lengkap'])
    no_telp = str(request.POST['no_telp'])
    jenis_kelamin = str(request.POST['jenis_kelamin'])
    tanggal_lahir = str(request.POST['tanggal_lahir'])
    testTGL = tanggal_lahir.split("-")
    if(len(testTGL)!=3):
        return registerPenggunaRole(request, "Format date yang diberikan tidak sesuai", "Konsumen")
    elif(len(testTGL[0])!=4):
        return registerPenggunaRole(request, "Format date yang diberikan tidak sesuai", "Konsumen")
    elif(len(testTGL[1])!=2):
        return registerPenggunaRole(request, "Format date yang diberikan tidak sesuai", "Konsumen")
    elif(len(testTGL[2])!=2):
        return registerPenggunaRole(request, "Format date yang diberikan tidak sesuai", "Konsumen")

    alamat = []
    status = []
    count_id = 0

    while True:
        try:
            count_id = count_id + 1
            id_alamat = "alamat"+ str(count_id)
            hasil1 = str(request.POST[id_alamat])
            alamat.append(hasil1)
            id_status = "status"+ str(count_id)
            hasil2 = str(request.POST[id_status])
            status.append(hasil2)
        except Exception as e:
            break
    
    panjang_alamat = len(alamat)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")

    cursor.execute("select * from pengguna where email='"+email+"'")
    hasil = namedtuplefetchall(cursor)
    if (hasil != []):
        cursor.close()
        return registerPenggunaRole(request, "Email sudah digunakan", "Konsumen")
    
    cursor.execute("select id_konsumen as c from konsumen order by id_konsumen::int asc")
    id_konsumen = namedtuplefetchall(cursor)
    id_konsumen = int(id_konsumen[-1].c)
    id_konsumen = id_konsumen + 1
    idk = str(id_konsumen)

    for i in range(panjang_alamat):
        for j in range(panjang_alamat):
            if (i!=j):
                if(alamat[i]==alamat[j] and status[i]==status[j]):
                    cursor.close()
                    return registerPenggunaRole(request, "Alamat dan status duplikat", "Konsumen")

    cursor.execute("insert into pengguna values ('"+email+"','"+no_telp+"','"+password+"','"+nama_lengkap+"')")
    cursor.execute("insert into konsumen values ('"+idk+"','"+email+"','"+jenis_kelamin+"','"+tanggal_lahir+"')")
    for i in range(panjang_alamat):
        cursor.execute("insert into alamat_konsumen values ('"+idk+"','"+alamat[i]+"','"+status[i]+"')")
                
    cursor.execute("select * from pengguna where email='"+email+"' and password='"+password+"'")
    hasil = namedtuplefetchall(cursor)

    if (hasil == []):
        cursor.close()
        return index(request, False)
    else:
        cursor.execute("set search_path to public")
        request.session['email'] = hasil[0].email
        request.session['telepon'] = hasil[0].telepon
        request.session['password'] = hasil[0].password
        request.session['nama_lengkap'] = hasil[0].nama_lengkap
        request.session.set_expiry(1800)
        request.session['role'] = 'Konsumen'
        argument = {
            'hasil' : hasil,
            'roleAdmin' : False,
            'roleKonsumen' : True,
            'roleKurir' : False,
            'roleCS' : False
        }
        cursor.close()
        return render(request, "home.html", argument)