from django import forms

class LoginForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50)
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128)

class ChoiceRoleForm(forms.Form):
    CHOICES=[
        ('Admin','Admin'),
        ('Konsumen','Konsumen'),
        ('Kurir','Kurir'),
        ('CS','CS')]
    Role = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)

class AdminRoleForm(forms.Form):
    email = forms.CharField(label = 'Email' , max_length=50)
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label = 'No Telepon', max_length=20)

class KonsumenRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50)
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label = 'No Telepon',max_length=20)
    jenis_kelamin = forms.CharField(label = 'Jenis Kelamin', max_length=1)
    tanggal_lahir = forms.CharField(label = 'Tanggal Lahir (yyyy-mm-dd)', max_length=10)

class KurirRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50)
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label = 'No Telepon', max_length=20)
    nama_perusahaan = forms.CharField(label = 'Nama Perusahaan', max_length=50)

class CSRoleForm(forms.Form):
    email = forms.CharField(label = 'Email', max_length=50)
    password = forms.CharField(label = 'Password',  widget=forms.PasswordInput, max_length=128)
    nama_lengkap = forms.CharField(label = 'Nama Lengkap', max_length=50)
    no_telp = forms.CharField(label = 'No Telepon', max_length=20)
    no_ktp = forms.CharField(label = 'No KTP', max_length=20)
    no_sia = forms.CharField(label = 'Nomor SIA', max_length=20)
    