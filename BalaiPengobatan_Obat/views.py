# Create your views here.
from django.shortcuts import render, reverse, redirect
from .forms import createBalaiPengobatanForms, updateBalaiPengobatanForms, createObatForm, updateObatForm
import pandas as pd
from django_tables2.tables import Table
from django.contrib.staticfiles import finders
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.auth.models import User, Permission
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, logout, login
from django.conf.urls.static import static
from .forms import *
from django.db import connection
from collections import namedtuple
from django.http import HttpResponseRedirect
from django.db import IntegrityError


#GENERAL FUNCTION
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

#INPUT obat
def get_input_obat(request):
    input = request.POST
    netto = input['netto']
    dosis = input['dosis']
    aturan_pakai = input['aturan_pakai']
    kontraindikasi = input['kontraindikasi']
    bentuk_kesediaan = input['bentuk_kesediaan']
    return [netto,dosis,aturan_pakai,kontraindikasi,bentuk_kesediaan]

#Create obat FUNCTION
def obatcreate(request):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_merk_obat from merk_obat order by cast(id_merk_obat as int)")
    id_merk_obat_view = namedtuplefetchall(cursor)

    if request.method =='POST':
        input = get_input_obat(request)
        id_merk_obat = request.POST['id_merk_obat']
        cursor = connection.cursor()
        cursor.execute("set search_path to farmakami")
        cursor.execute("select id_obat as c from obat order by id_obat::int asc")
        id_obat = namedtuplefetchall(cursor)
        id_obat = id_obat[-1].c
        cursor.execute("select id_produk as c from obat order by id_produk::int asc")
        id_produk = namedtuplefetchall(cursor)
        id_produk = id_produk[-1].c
        id_obat = int(id_obat) + 1
        idString1 = str(id_obat)
        id_produk = int(id_produk) + 1
        idString2 = str(id_produk)
        
        try :
            cursor.execute("insert into obat values ('"+idString1+"','"+idString2+"','"+id_merk_obat+"','"+input[0]+"','"+input[1]+"','"+input[2]+"','"+input[3]+"','"+input[4]+"')")
            cursor.close()
            return redirect('/obat/listObat')
        except:
            message = "Data tidak berhasil dibuat"

    form = createObatForm()
    
    args = {
        'id_merk_obat': id_merk_obat_view,
        'form': form,
        'message' : message
        }
    return render(request, "createObat.html", args)

#READ obat FUNCTION
def obatlist(request):
    cursor = connection.cursor()
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
    except:
        return redirect('/')

    email = str(request.session['email'])
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from obat order by cast(id_obat as int)")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "listObat.html", argument)

#UPDATE obat FUNCTION
def updateobat(request, id_obat, id_produk, id_merk_obat, success=True):
    message =""
    id_obat = str(id_obat)
    id_produk = str(id_produk)
    id_merk_obat = str(id_merk_obat)
    id_merk_obat_new = str(id_merk_obat)
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_merk_obat from merk_obat order by cast(id_merk_obat as int)")
    id_merk_obat_view = namedtuplefetchall(cursor)
    cursor.execute("select * from obat where id_obat = '"+id_obat+"' and id_produk = '"+id_produk+"'")
    hasil = namedtuplefetchall(cursor)
    id_obat = hasil[0].id_obat
    id_produk = hasil[0].id_produk
    netto = hasil[0].netto
    dosis = hasil[0].dosis
    aturan_pakai = hasil[0].aturan_pakai
    kontraindikasi = hasil[0].kontraindikasi
    bentuk_kesediaan = hasil[0].bentuk_kesediaan
    
    form = updateObatForm(initial={
        'id_obat': id_obat, 'id_produk': id_produk, 'netto': netto, 'dosis': dosis, 'aturan_pakai': aturan_pakai, 'kontraindikasi': kontraindikasi, 'bentuk_kesediaan': bentuk_kesediaan })
    if request.method == 'POST':
        input = get_input_obat(request)
        id_merk_obat_new = request.POST['id_merk_obat']
        print(input)
        try :
            cursor.execute("update obat set id_merk_obat ='"+id_merk_obat_new+"', netto ='"+input[0]+"', dosis ='"+input[1]+"', aturan_pakai ='"+input[2]+"', kontraindikasi ='"+input[3]+"', bentuk_kesediaan ='"+input[4]+"' where id_obat = '"+id_obat+"' and id_produk = '"+id_produk+"'")
            cursor.close()
            message = "Data berhasil diubah"
            return redirect('/obat/listObat')
        except:
            message = "Data tidak berhasil diubah"
            

    
    argument = {
        'form' : form,
        'message' : message,
        'id_obat' : id_obat,
        'id_produk' : id_produk,
        'idMerkObatForm' : id_merk_obat,
        'id_merk_obat': id_merk_obat_view,
    }
    return render(request, "updateObat.html", argument)

#DELETE obat FUNCTION
def deleteobat(request, id_obat, id_produk):
    message =""
    id_obat = str(id_obat)
    id_produk = str(id_produk)
    cursor = connection.cursor()
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
    except:
        return redirect('/')


    email = str(request.session['email'])
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from obat where id_obat ='"+id_obat+"' and id_produk = '"+id_produk+"'")
    cursor.execute("select * from obat order by cast(id_obat as int)")
    hasil = namedtuplefetchall(cursor)
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'message' : message,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "listObat.html", argument)

#INPUT balai_pengobatan
def get_input_balai_pengobatan(request):
    input = request.POST
    alamat_balai = input['alamat_balai']
    nama_balai = input['nama_balai']
    jenis_balai = input['jenis_balai']
    telepon_balai = input['telepon_balai']
    return [alamat_balai,nama_balai,jenis_balai,telepon_balai]

#CREATE balai_pengobatan FUNCTION
def bpcreate(request):
    message =""
    if request.method =='POST':
        input = get_input_balai_pengobatan(request)
        form = createBalaiPengobatanForms(request.POST)
        if form.is_valid():
            cursor = connection.cursor()
            cursor.execute("set search_path to farmakami")
            cursor.execute("select id_balai as c from balai_pengobatan order by id_balai::int asc")
            id_balai = namedtuplefetchall(cursor)
            id_balai = id_balai[-1].c
            id_balai = int(id_balai) + 1
            idString = str(id_balai)
            id_apotek = form.cleaned_data['id_apotek']
            
            try :
                cursor.execute("insert into balai_pengobatan values ('"+idString+"','"+input[0]+"','"+input[1]+"','"+input[2]+"','"+input[3]+"')")
                cursor.execute("INSERT INTO balai_apotek VALUES (%s, %s)",
                        [id_balai, id_apotek])
                cursor.close()
                return redirect('/balaipengobatan/listBalaiPengobatan')
            except:
                message = "Data tidak berhasil dibuat"

    form = createBalaiPengobatanForms()
    args = {
        'form': form,
        'message' : message
        }
    return render(request, 'createBalaiPengobatan.html', args)

#READ balai_pengobatan FUNCTION
def bplist(request):
    cursor = connection.cursor()
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
    except:
        return redirect('/')

    email = str(request.session['email'])

    cursor.execute("set search_path to farmakami")
    cursor.execute("select p.id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai, id_apotek from balai_pengobatan as p left join balai_apotek as a on a.id_balai = p.id_balai order by p.id_balai::int asc")
    hasil = namedtuplefetchall(cursor)
    # cursor.execute("SELECT id_apotek FROM BALAI_PENGOBATAN as bp JOIN BALAI_APOTEK as ba ON bp.id_balai = ba.id_balai")
    # id_apotek_view = namedtuplefetchall(cursor)
    cursor.close()
    argument = {
        'table' : hasil,
        # 'id_apotek' : id_apotek_view,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "listBalaiPengobatan.html", argument)

#UPDATE balai_pengobatan FUNCTION
def updatebp(request, id_balai, message=None):
    message =""
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from balai_pengobatan where id_balai = '"+id_balai+"'")
    hasil = namedtuplefetchall(cursor)
    alamat_balai = hasil[0].alamat_balai
    nama_balai = hasil[0].nama_balai
    jenis_balai = hasil[0].jenis_balai
    telepon_balai = hasil[0].telepon_balai
    cursor.execute("SELECT id_apotek FROM BALAI_PENGOBATAN as bp JOIN BALAI_APOTEK as ba ON bp.id_balai = ba.id_balai WHERE bp.id_balai = %s", [id_balai])
    id_apotek = namedtuplefetchall(cursor)
    print(id_apotek)
    
    form = updateBalaiPengobatanForms(initial={
        'id_balai': id_balai, 'alamat_balai': alamat_balai, 'nama_balai': nama_balai,  'jenis_balai': jenis_balai, 'telepon_balai': telepon_balai, 'id_apotek': id_apotek})
    if request.method == 'POST':
        input = get_input_balai_pengobatan(request)
        id_apotek_new = request.POST['id_apotek']
        print(id_balai)

        try :
            cursor.execute("update balai_pengobatan set id_balai ='"+id_balai+"', alamat_balai ='"+input[0]+"', nama_balai ='"+input[1]+"', jenis_balai ='"+input[2]+"', telepon_balai ='"+input[3]+"' where id_balai ='"+id_balai+"'")
            if (id_apotek==[]):
                cursor.execute("insert into balai_apotek values ('"+id_balai+"', '"+id_apotek_new+"')")
                cursor.close()
                return redirect('/balaipengobatan/listBalaiPengobatan')
            else:
                cursor.execute("update balai_apotek set id_apotek = '"+str(id_apotek_new)+"' where id_balai = '"+id_balai+"'")
                return redirect('/balaipengobatan/listBalaiPengobatan')
        except:
            message = "Data tidak berhasil DIBUAT/DIUBAH karena 'Alamat Balai' yang sama sudah terdapat di tabel"
      
    argument = {
        'form' : form,
        'message' : message,
        'id_balai' : id_balai,
    }
    return render(request, "updateBalaiPengobatan.html", argument)
    # return updatebp(request, id_balai)

#DELETE balai_pengobatan FUNCTION
def deletebp(request, id_balai):
    id_balai = str(id_balai)
    cursor = connection.cursor()
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
    except:
        return redirect('/')

    email = str(request.session['email'])
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from balai_pengobatan where id_balai ='"+id_balai+"'")
    cursor.execute("select p.id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai, id_apotek from balai_pengobatan as p left join balai_apotek as a on a.id_balai = p.id_balai order by p.id_balai::int asc")
    hasil = namedtuplefetchall(cursor)
    cursor.execute("DELETE FROM BALAI_APOTEK WHERE id_balai = %s", [id_balai])
    cursor.close()
    message = "Data berhasil dihapus"
    argument = {
        'table' : hasil,
        'message' : "Data berhasil dihapus",
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    return render(request, "listBalaiPengobatan.html", argument)

