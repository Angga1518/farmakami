from django.shortcuts import render, redirect
from .forms import UpdateTransaksiPembelianForm
from django.db import connection
from collections import namedtuple
from random import randrange

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def readTransaksiPembelian(request):
    cursor = connection.cursor()
    roleAdmin = False
    roleKonsumen = False
    roleKurir = False
    roleCS = False
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
        if (request.session['role'] == 'Konsumen'):
            roleKonsumen = True
        if (request.session['role'] == 'Kurir'):
            roleKurir = True
        if (request.session['role'] == 'CS'):
            roleCS = True
    except:
        return redirect('/')

    email = str(request.session['email'])

    cursor.execute("set search_path to farmakami")
    if(roleKonsumen):
        cursor.execute("select id_konsumen from konsumen where email = '"+email+"'")
        idk = namedtuplefetchall(cursor)
        idk = str(idk[0].id_konsumen)
        cursor.execute("select * from transaksi_pembelian where id_konsumen = '"+idk+"' order by id_transaksi_pembelian::int asc")
        hasil = namedtuplefetchall(cursor)
    else:
        cursor.execute("select * from transaksi_pembelian order by id_transaksi_pembelian::int asc")
        hasil = namedtuplefetchall(cursor)

    argument = {
        'hasil' : hasil,
        'roleAdmin' : roleAdmin,
        'roleKonsumen' : roleKonsumen,
        'roleKurir' : roleKurir,
        'roleCS' : roleCS
    }
    cursor.close()
    return render(request, "read.html", argument)

def formUpdateTransaksiPembelian(request, id_transaksi_pembelian, success=True):
    cursor = connection.cursor()
    id_transaksi_pembelian = str(id_transaksi_pembelian)
    cursor.execute("set search_path to farmakami")
    cursor.execute("select * from transaksi_pembelian where id_transaksi_pembelian ='"+id_transaksi_pembelian+"'")
    hasil = namedtuplefetchall(cursor)
    waktu_pembelian = str(hasil[0].waktu_pembelian)
    total_pembayaran = str(hasil[0].total_pembayaran)
    
    formulir = UpdateTransaksiPembelianForm(initial={'id_transaksi_pembelian': id_transaksi_pembelian, 'waktu_pembelian': waktu_pembelian, 'total_pembayaran': total_pembayaran})

    cursor.execute("select id_konsumen from konsumen order by id_konsumen::int asc")
    opt = namedtuplefetchall(cursor)
    argument = {
        'form' : formulir,
        'opt' : opt,
    }
    cursor.close()
    return render(request, "formUpdate.html", argument)

def updateTransaksiPembelian(request):
    id_transaksi_pembelian = request.POST['id_transaksi_pembelian']
    id_konsumen = request.POST['id_konsumen_select']

    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select now()")
    hasil = namedtuplefetchall(cursor)
    
    cursor.execute("update transaksi_pembelian set id_konsumen = '"+id_konsumen+"' where id_transaksi_pembelian = '"+id_transaksi_pembelian+"'")
    cursor.execute("update transaksi_pembelian set waktu_pembelian = '"+str(hasil[0].now)+"' where id_transaksi_pembelian = '"+id_transaksi_pembelian+"'")

    cursor.execute("select * from transaksi_pembelian order by id_transaksi_pembelian::int")
    hasil = namedtuplefetchall(cursor)
    message = "Data berhasil di update"

    roleAdmin = False
    cursor.execute("set search_path to public")
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")

    argument = {
        'hasil' : hasil,
        'message' : message,
        'roleAdmin' : roleAdmin,
    }
    cursor.close()
    return render(request, "read.html", argument)

def deleteTransaksiPembelian(request, id_transaksi_pembelian):
    cursor = connection.cursor()
    id_transaksi_pembelian = str(id_transaksi_pembelian)
    cursor.execute("set search_path to farmakami")
    cursor.execute("delete from transaksi_pembelian where id_transaksi_pembelian ='"+id_transaksi_pembelian+"'")
    cursor.execute("select * from transaksi_pembelian order by id_transaksi_pembelian::int")
    hasil = namedtuplefetchall(cursor)
    # gaboleh lewatin constraint

    roleAdmin = False
    cursor.execute("set search_path to public")
    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')
    cursor.execute("set search_path to farmakami")
    argument = {
        'hasil' : hasil,
        'message' : "Data berhasil dihapus",
        'roleAdmin' : roleAdmin,
    }
    cursor.close()
    return render(request, "read.html", argument)

def formCreateTransaksiPembelian(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select id_konsumen from konsumen order by id_konsumen::int asc")
    opt = namedtuplefetchall(cursor)
    argument = {
        'opt' : opt,
    }
    cursor.close()
    return render(request, "formCreate.html", argument)

def createTransaksiPembelian(request):
    id_konsumen = request.POST['id_konsumen_select']
    cursor = connection.cursor()
    cursor.execute("set search_path to farmakami")
    cursor.execute("select Now() as n")
    waktu_pembelian = namedtuplefetchall(cursor)
    waktu_pembelian = str(waktu_pembelian[0].n)
    cursor.execute("select id_transaksi_pembelian as c from transaksi_pembelian order by id_transaksi_pembelian::int asc")
    id_transaksi_pembelian = namedtuplefetchall(cursor)
    id_transaksi_pembelian = int(id_transaksi_pembelian[-1].c)

    id_transaksi_pembelian = id_transaksi_pembelian + 1
    idt = str(id_transaksi_pembelian)
    cursor.execute("insert into transaksi_pembelian (id_transaksi_pembelian,waktu_pembelian,id_konsumen) values ('"+idt+"','"+waktu_pembelian+"','"+id_konsumen+"')")

    cursor.execute("select * from transaksi_pembelian order by id_transaksi_pembelian::int")
    hasil = namedtuplefetchall(cursor)
    message = "Data berhasil dibuat"
    roleAdmin = False
    cursor.execute("set search_path to public")

    try:
        if (request.session['role'] == 'Admin'):
            roleAdmin = True
    except:
        return redirect('/')

    cursor.execute("set search_path to farmakami")
    argument = {
        'hasil' : hasil,
        'message' : message,
        'roleAdmin' : roleAdmin,
    }
    cursor.close()
    return render(request, "read.html", argument)


