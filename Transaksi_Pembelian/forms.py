from django import forms

class UpdateTransaksiPembelianForm(forms.Form):
    id_transaksi_pembelian = forms.CharField(label = 'Id Transaksi Pembelian', widget = forms.TextInput(attrs={'readonly':'readonly'}))
    waktu_pembelian = forms.DateTimeField(label = 'Waktu Pembelian',  widget = forms.TextInput(attrs={'readonly':'readonly'}))
    total_pembayaran = forms.DateTimeField(label = 'Total Pembayaran',  widget = forms.TextInput(attrs={'readonly':'readonly'}))


